﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Models
{
    [Table("Vacancy")]
    public class VacancyModel
    {
        [Key]
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Title { get; set; }

        public string Oraganization { get; set; }

        public string OraganizationLink { get; set; }

        public string Address { get; set; }

        public string Salary { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string ContactName { get; set; }
    }
}
