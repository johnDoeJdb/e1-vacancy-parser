﻿using System.Collections.Generic;
using System.Threading;

namespace VacancyCatalog.Extensions
{
    static class ThreadExtension
    {
        public static void WaitAll(this IEnumerable<Thread> threads)
        {
            if (threads != null)
            {
                foreach (Thread thread in threads)
                { thread.Join(); }
            }
        }

        public static void StartAll(this IEnumerable<Thread> threads)
        {
            if (threads != null)
            {
                foreach (Thread thread in threads)
                { thread.Start(); }
            }
        }
    }
}
