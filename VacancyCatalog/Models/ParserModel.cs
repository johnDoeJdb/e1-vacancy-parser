﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DataLayer.Models;
using JobParserE1.Category.Model;
using VacancyCatalog.Extensions;

namespace VacancyCatalog.Models
{
    public class ParserModel
    {
        protected delegate void GetChildren(Category category, int parentId = 0);
        protected delegate void AddParserThread(int categoryId);
        protected List<VacancyModel> ListVacancies;
        protected int ThreadLimit = 5;

        public List<VacancyModel> StartVacancyParser()
        {
            var parserThreads = new List<Thread>();
            var categories = GetCategories();
            var limit = 4;
            ListVacancies = new List<VacancyModel>();
            foreach (var categoryId in categories)
            {
                parserThreads.Add(CreateParserThread(categoryId));
                if (parserThreads.Count > ThreadLimit)
                {
                    parserThreads.StartAll();
                    parserThreads.WaitAll();
                    using (var db = new ApplicationDbContext())
                    {
                        db.VacancyModels.AddRange(ListVacancies);
                        db.SaveChanges();
                        ListVacancies.Clear();
                    }
                    parserThreads.Clear();
                }
            }

            return new List<VacancyModel>();
        }

        protected int[] GetCategories()
        {
            int[] categories;
            using (var db = new ApplicationDbContext())
            {
                if (!db.CategoryModels.Any())
                {
                    var category = JobParserE1.Category.CategoryParser.GetGategories();
                    var categoryModel = ConvertToCategoryModel(category);
                    db.CategoryModels.AddRange(categoryModel);
                    db.SaveChanges();
                }
                categories = db.CategoryModels.Where(c => c.ParentId != 0).Select(c => c.Id).ToArray();
            }

            return categories;
        }

        protected Thread CreateParserThread(int categoryId)
        {
            AddParserThread addParserThread = delegate (int categoryIdInner)
            {
                int[] parsedIds;
                using (var db = new ApplicationDbContext())
                {
                    parsedIds = db.VacancyModels.Where(u => u.CategoryId.Equals(categoryIdInner)).Select(v => v.Id).ToArray();
                }
                var listVacancies = JobParserE1.Vacancy.VacancyParser.ParseVacancyByCategoryId(categoryIdInner, parsedIds);
                var listVacanciesModel = ConvertToVacancyModel(listVacancies);
                ListVacancies.AddRange(listVacanciesModel);
            };
            var thread = new Thread(() => addParserThread(categoryId));

            return thread;
        }

        protected List<CategoryModel> ConvertToCategoryModel(Category categoryTempModel)
        {
            GetChildren getChildren = null;
            var categories = new List<CategoryModel>();
            getChildren = delegate (Category category, int parentId)
            {
                if (category.ChildCategories != null && category.ChildCategories.Any())
                {
                    foreach (var childCategory in category.ChildCategories)
                    {
                        getChildren(childCategory, category.Id);
                    }
                }
                else
                {
                    var categoryModel = new CategoryModel
                    {
                        Id = category.Id,
                        Title = category.Title,
                        ParentId = parentId
                    };
                    categories.Add(categoryModel);
                }
            };
            getChildren(categoryTempModel, 0);

            return categories;
        }

        protected List<VacancyModel> ConvertToVacancyModel(List<JobParserE1.Vacancy.Model.VacancyModel> listVacancies)
        {
            var vacanciesList = new List<VacancyModel>();
            foreach (var vacancy in listVacancies)
            {
                var vacancyModel = new VacancyModel()
                {
                    Id = vacancy.Id,
                    Address = vacancy.Address,
                    CategoryId = vacancy.CategoryId,
                    ContactName = vacancy.ContactName,
                    Oraganization = vacancy.CompanyTitle,
                    OraganizationLink = vacancy.CompanyLink,
                    Description = vacancy.Description,
                    Title = vacancy.Title,
                    Phone = vacancy.Phone,
                    Salary = vacancy.Salary
                };
                vacanciesList.Add(vacancyModel);
            }

            return vacanciesList;
        }
    }
}