﻿using System.Collections.Generic;
using DataLayer.Models;

namespace VacancyCatalog.Models
{
    public class CatalogViewModel 
    {
        public IEnumerable<VacancyModel> Vacancies { get; set; }
    }
}