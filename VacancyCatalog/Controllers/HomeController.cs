﻿using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using DataLayer.Models;
using VacancyCatalog.Models;

namespace VacancyCatalog.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StartParser()
        {
            var parserModel = new ParserModel();
            var thread = new Thread(() => parserModel.StartVacancyParser());
            thread.Start();

            return RedirectToAction("Parser");
        }

        public ActionResult Catalog()
        {
            var catalogViewModel = new CatalogViewModel();
            using (var db = new ApplicationDbContext())
            {
                catalogViewModel.Vacancies = db.VacancyModels.ToArray();
            }

            return View(catalogViewModel);
        }

        public ActionResult Parser()
        {
            return View();
        }
    }
}