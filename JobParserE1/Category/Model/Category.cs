﻿using System.Collections.Generic;

namespace JobParserE1.Category.Model

{
    public class Category
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public List<Category> ChildCategories { get; set; }
    }
}
