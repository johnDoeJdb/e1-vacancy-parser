﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using RestSharp;

namespace JobParserE1.Category
{
    public class CategoryParser
    {
        public static Model.Category GetGategories()
        {
            var client = new RestClient(Urls.E1Host);
            var request = new RestRequest(Urls.E1GetGategoriesPath, Method.GET);
            request.AddHeader("Referer", string.Format("{0}{1}", Urls.E1Host, Urls.E1GetGategoriesRefererPath));
            request.AddHeader("X-Requested-With", "XMLHttpRequest");

            var response = client.Execute(request);
            var htmlContent = response.Content; 
            var categories = ParserHtmlCategories(htmlContent);

            return categories;
        }

        protected static Model.Category ParserHtmlCategories(string html)
        {
            var rootCategory = new Model.Category();
            rootCategory.Title = "root";
            rootCategory.Id = 0;
            rootCategory.ChildCategories = new List<Model.Category>(); ;

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            foreach (var div in htmlDocument.DocumentNode.SelectNodes("//div[contains(@class,'msel-0')]"))
            {
                rootCategory = ParseOneCategory(div, rootCategory);               
            }

            return rootCategory;
        }

        protected static Model.Category ParseOneCategory(HtmlNode category, Model.Category vacancyCategory)
        {
            var parentCategory = new Model.Category();
            var parentNode = category.SelectNodes("div[contains(@class,'msel-item')]").First();
            var parentCategoryId = parentNode.SelectNodes("input").First().Attributes["value"].Value;
            parentCategory.Id = int.Parse(parentCategoryId);
            parentCategory.Title = parentNode.SelectNodes("label").First().InnerText.Trim();
            parentCategory.ChildCategories = new List<Model.Category>();

            var childNodes = category.SelectNodes("div[contains(@class,'msel-1')]");
            foreach (var childNode in childNodes)
            {
                var itemId = childNode.SelectNodes("div/input").First().Attributes["value"].Value;
                var itemTitle = childNode.SelectNodes("div/label").First().InnerText.Trim();
                var childCategory = new Model.Category();
                childCategory.Id = int.Parse(itemId);
                childCategory.Title = itemTitle;
                parentCategory.ChildCategories.Add(childCategory);
            }
            
            vacancyCategory.ChildCategories.Add(parentCategory);

            return vacancyCategory;
        }
    }
}
