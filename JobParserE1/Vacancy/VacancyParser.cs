﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HtmlAgilityPack;
using JobParserE1.Vacancy.Model;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using RestSharp;
using RestSharp.Contrib;

namespace JobParserE1.Vacancy
{
    public class VacancyParser
    {
        public static List<VacancyModel> ParseVacancyByCategoryId(int id, int[] parserIds)
        {
            var vacancies = new List<VacancyModel>();
            var nextPagePath = string.Format("{0}{1}", Urls.E1GetRubricPath, id);
            while (nextPagePath != null)
            {
                var client = new RestClient(Urls.E1Host);
                var request = new RestRequest(nextPagePath, Method.GET);
                var response = client.Execute(request);
                var htmlContent = response.Content; 
                vacancies.AddRange(ParseHtmlPage(htmlContent, id, parserIds));
                nextPagePath = GetNextPageUrl(htmlContent);
            }

            return vacancies;
        }

        protected static List<VacancyModel> ParseHtmlPage(string html, int categoryId, int[] parserIds)
        {
            var vacancies = new List<VacancyModel>();
            var rootCategory = new Category.Model.Category();
            rootCategory.Title = "root";
            rootCategory.Id = 0;
            rootCategory.ChildCategories = new List<Category.Model.Category>(); ;

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            var vacancyListNode = htmlDocument.DocumentNode.SelectNodes("//ul[contains(@class,'ra-elements-list-hidden')]/li");
            foreach (var item in vacancyListNode)
            {
                var path = item.SelectNodes("a[1]").First().Attributes["href"].Value;
                var uri = new Uri(string.Format("{0}{1}", Urls.E1Host, path));
                var id = int.Parse(HttpUtility.ParseQueryString(uri.Query).Get("id"));
                if (parserIds.Contains(id))
                {
                    break;
                }
                vacancies.Add(ParseVacancy(path, categoryId, id));
            }

            return vacancies;
        }

        protected static VacancyModel ParseVacancy(string path, int categoryId, int vacancyId)
        {
            var htmlContent = LoadHtml(string.Format("{0}{1}", Urls.E1Host, path));
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlContent);

            string companyTitle = null;
            string companyLink = null;
            string address = null;
            string description = null;
            string salary = null;

            var title = htmlDocument.DocumentNode.SelectNodes("//a[contains(@class,'ra-elements-list__title__link')]").First().InnerText.Trim();

            var companyNode = htmlDocument.DocumentNode.SelectNodes("//div[contains(@class,'ra-elements-list__subtitle')]/a").FirstOrDefault();
            if (companyNode != null)
            {
                companyTitle = companyNode.InnerText.Trim();
                companyLink = string.Format("{0}{1}", Urls.E1Host, companyNode.Attributes["href"].Value);
            }

            var addressNode = htmlDocument.DocumentNode.SelectNodes("//div[contains(@class,'ra-elements-list__brief')]").FirstOrDefault();
            if (addressNode != null)
            {
                addressNode = addressNode.SelectNodes("text()").FirstOrDefault();
                if (addressNode != null)
                {
                    address = addressNode.InnerText.Trim();
                }
            }

            var descriptionNode = htmlDocument.DocumentNode.SelectNodes("//div[contains(@class,'ra-elements-list__content-full')]").FirstOrDefault();
            if (descriptionNode != null)
            {
                description = descriptionNode.InnerHtml;
            }

            var salaryNode = htmlDocument.DocumentNode.SelectNodes("//div[contains(@class,'ra-elements-list__pay ')]").FirstOrDefault();
            if (salaryNode != null)
            {
                salary = salaryNode.InnerText.Trim();
            }
           
            var vacancy = new VacancyModel
            {
                Id = vacancyId,
                Title = title,
                CompanyTitle = companyTitle,
                CompanyLink = companyLink,
                Address = address,
                Description = description,
                Salary = salary,
                CategoryId = categoryId
            };

            return vacancy;
        }

        protected static string GetNextPageUrl(string htmlPage)
        {
            string nextPageUrl = null;
            int lastPage;
            int currentPage;

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlPage);
            var totalPageNumbers = htmlDocument.DocumentNode.SelectNodes("//ul[contains(@class,'ra-pagination-pages')]/li").Count();
            var lastPageString = htmlDocument.DocumentNode.SelectNodes("//ul[contains(@class,'ra-pagination-pages')]/li").Last().InnerText.Trim();
            if (int.TryParse(lastPageString, out lastPage))
            {
                lastPageString =
                    htmlDocument.DocumentNode.SelectNodes(
                        string.Format("//ul[contains(@class,'ra-pagination-pages')]/li[{0}]", totalPageNumbers - 1))
                        .First()
                        .InnerText;
                int.TryParse(lastPageString, out lastPage);
            }
            else
            {
                return null;
            }

            var currentPageString = htmlDocument.DocumentNode.SelectNodes("//ul[contains(@class,'ra-pagination-pages')]/li[contains(@class,'active')]").First().InnerText;
            int.TryParse(currentPageString, out currentPage);

            if (lastPage != currentPage)
            {
                nextPageUrl = htmlDocument.DocumentNode.SelectNodes("//ul[contains(@class,'ra-pagination-pages')]/li/a").First().Attributes["href"].Value;
            }

            return nextPageUrl;
        }

        protected static string LoadHtml(string url)
        {
            var phantomDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\packages\PhantomJS.1.9.2\tools\phantomjs");
            string html;
            using (IWebDriver phantomDriver = new PhantomJSDriver(phantomDirectory))
            {
                phantomDriver.Url = url;
                html = phantomDriver.PageSource;
            }

            return html;
        }
    }
}
