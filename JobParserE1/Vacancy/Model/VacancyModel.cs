﻿using System;

namespace JobParserE1.Vacancy.Model

{
    public class VacancyModel
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        public string Title { get; set; }

        public string CompanyTitle { get; set; }

        public string CompanyLink { get; set; }

        public string Address { get; set; }

        public string Salary { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string ContactName { get; set; }
    }
}
